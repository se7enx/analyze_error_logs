<?php
/**
 * @package ContextualCode\AnalyzeErrorLogs
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 **/

namespace ContextualCode\AnalyzeErrorLogs;

class Helper {
    public static function sendEmail($receivers, $resultsDir, $subject, $text) {
        $uid = md5(uniqid(time()));
        $mimeBoundary = '==Multipart_Boundary_x' . $uid . 'x';
        $separator = "\r\n";

        $headers = 'MIME-Version: 1.0' . $separator;
        $headers .= 'Content-Type: multipart/mixed; boundary="' . $uid . '"' . $separator . $separator;

        $message = '--' . $uid . $separator;
        $message .= 'Content-Type: text/plain; charset=iso-8859-1' . $separator;
        $message .= 'Content-Transfer-Encoding: base64' . $separator . $separator;
        $message .= chunk_split(base64_encode($text)) . $separator . $separator;

        $send    = false;
        $results = scandir($resultsDir);
        foreach($results as $file) {
            if($file == '.' || $file == '..') {
                continue;
            }

            $send = true;

            $filename = basename($file);
            $content = file_get_contents($resultsDir . '/' . $file);
            $content = chunk_split(base64_encode($content));

            $message .= '--' . $uid . $separator;
            $message .= 'Content-Type: application/octet-stream; name="' . $filename . '"' . $separator;
            $message .= 'Content-Transfer-Encoding: base64' . $separator;
            $message .= 'Content-Disposition: attachment' . $separator;
            $message .= 'X-Attachment-Id: ' . rand(1000, 99999) . $separator . $separator;
            $message .= $content . $separator;
        }

        if( $send ) {
            mail(implode(', ', $receivers), $subject, $message, $headers);
        }
    }
}
