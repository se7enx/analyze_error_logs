<?php
/**
 * @package ContextualCode\AnalyzeErrorLogs
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 **/

namespace ContextualCode\AnalyzeErrorLogs;

require_once(__DIR__ . '/BaseAnalyzer.php');

class DockerFpmPhpAnalyzer extends BaseAnalyzer {
    protected $files = array(
        'remote'  => null,
        'local'   => null,
        'results' => null
    );
    protected $logDateFormat = 'd-M-Y H:i:s T';

    public function __construct() {
        parent::__construct();
        $this->files['local'] = '/tmp/docker-fpm-php.log';
        $this->files['remote'] = '/var/log/fpm-php.www.log';
    }

    protected function getLogLines() {
        exec('rm ' . $this->files['local']);
        exec('docker cp $(sudo docker ps | grep ezp | awk \'{print $1}\'):' . $this->files['remote'] . ' ' . $this->files['local']);

        if(file_exists($this->files['local']) === false) {
            $this->error('Error copying "' . $this->files['remote'] . '" to "' . $this->files['local'] . '"');
        } else {
            $this->output('Log file "' . $this->files['remote'] . '" copied to "' . $this->files['local'] . '"');
        }

        return file($this->files['local']);
    }
}
