<?php
/**
 * @package ContextualCode\AnalyzeErrorLogs
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 **/

namespace ContextualCode\AnalyzeErrorLogs;

require_once(__DIR__ . '/BaseAnalyzer.php');

class NginxAnalyzer extends BaseAnalyzer {
    private static $suffix = '_error.log';
    private static $dir    = '/var/log/nginx';

    protected $logDateFormat = 'Y/m/d H:i:s';
    protected $regExps = array(
        // Replace PID
        '/ (\d+)\#(\d+)\: \*(\d+) /i' => ' ',
        '/Content-Length of (\d+) bytes exceeds the limit of/i' => 'Content-Length of XXX bytes exceeds the limit of',
        '/client intended to send too large body: (\d+) bytes/i' => 'client intended to send too large body: XXX bytes',
        // Ignore 404
        '/(.*)2: No such file or directory(.*)/i' => '',
        '/(.*)directory index of (.*) is forbidden(.*)/i' => '',
        // http://issuetrack.contextualcode.com/view.php?id=24461
        '/(.*)Call to a member function getTimestamp\(\) on null in \/srv\/www\/(\w+)\/vendor\/ezsystems\/ezpublish-kernel\/eZ\/Bundle\/EzPublishIOBundle\/BinaryStreamResponse\.php(.*)/i' => '',
        // Machform mysql_connect
        '/(.*)use mysqli or PDO instead in (.*)\/src\/ThinkCreative\/MachformBundle\/Lib\/machform\/includes\/db-core\.php on line 18(.*)/i' => '',
        // 2014.11 Strict standards error with Symfony
        '/(.*)Declaration of(.*)Controller::isGranted(.*)should be compatible(.*)/i' => ''
    );

    public function run() {
        $resultsBaseDir = dirname($this->files['results']) . '/';

        $logFiles = scandir(self::$dir);
        foreach($logFiles as $logFile) {
            if(strrpos($logFile,self::$suffix) === false || $logFile == 'default_error.log') {
                continue;
            }

            $this->files['local'] = self::$dir . '/' . $logFile;

            $vhost = str_replace(self::$suffix,'',basename($logFile));
            if(empty($vhost)) {
                continue;
            }
            $this->setResultsFile($resultsBaseDir . 'nginx_' . $vhost . '.log');

            parent::run();
        }
    }

    protected function extractDataFromLogLine($line) {
        $separator = ' ';
        $tmp       = explode($separator, $line);
        if(count($tmp) < 3) {
            return null;
        }

        $date    = implode($separator, array_slice($tmp,0,2));
        $error   = trim(implode($separator, array_slice($tmp,2)));

        return array(
            'date'  => $date,
            'error' => $error
        );
    }
}
